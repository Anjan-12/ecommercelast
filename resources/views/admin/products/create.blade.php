@extends('adminlte::page')

@section('title', 'Add New Product')

@section('plugins.Select2', true)

@section('js')
    <script>
        $(document).ready(function () {
            $('#categories').select2();

        });
    </script>
@endsection

@section('content')

    <x-alert/>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size:1.5em;font-weight: bold">Add New Product</h3>
            <div class="card-tools">
                <a class="btn btn-success btn-sm" href="{{ route('admin.products.index') }}">
                    <i class="fas fa-arrow-left fa-fw mr-1"></i> Go Back
                </a>
            </div>
        </div>
        <form action="{{ route('admin.products.store') }}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="sku" title="Stock Keeping Unit">SKU <span title="Required" class="text-danger">*</span></label>
                    <input type="text" name="sku" id="sku" class="form-control @error('sku') is-invalid @endif"
                           value="{{ old('sku') ?? "" }}" autofocus>

                    @error('sku')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="name">Name <span title="Required" class="text-danger">*</span></label>
                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @endif"
                           value="{{ old('name') ?? "" }}">

                    @error('name')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="slug">Slug <span title="Required" class="text-danger">*</span></label>
                    <input type="text" name="slug" id="slug" class="form-control @error('slug') is-invalid @endif"
                           value="{{ old('slug') ?? "" }}">

                    @error('slug')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="name">Description</label>
                    <textarea name="description" id="description"
                              class="form-control @error('description') is-invalid @endif">{{ old('description') ?? "" }}</textarea>

                    @error('description')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="price">Price <span title="Required" class="text-danger">*</span></label>
                    <input type="number" name="price" id="price" class="form-control @error('price') is-invalid @endif"
                           value="{{ old('price') ?? "" }}">

                    @error('price')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="icheck-primary">
                        <input type="checkbox" name="on_sale" id="on_sale" value="1" @if(old('on_sale')) checked @endif>
                        <label for="on_sale">Product is on sale?</label>
                    </div>

                    @error('on_sale')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="sale_price">Sale Price</label>
                    <input type="number" name="sale_price" id="sale_price"
                           class="form-control @error('sale_price') is-invalid @endif"
                           value="{{ old('sale_price') ?? "" }}">

                    @error('sale_price')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <h3 class="card-title" style="font-size:1.5em;font-weight: bold">Extra Details</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="min_order_quantity">Minimum Order Quantity</label>
                    <input type="text" name="min_order_quantity" id="min_order_quantity"
                           class="form-control @error('min_order_quantity') is-invalid @endif"
                           value="{{ old('min_order_quantity') ?? 0 }}">

                    @error('min_order_quantity')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="icheck-primary">
                        <input type="checkbox" name="on_stock" id="on_stock" value="1" checked>
                        <label for="on_stock">Product is on Stock?</label>
                    </div>

                    @error('on_stock')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>



                    @error('is_adult')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group row mb-0">
                    <div class="form-group col-12 col-md-6">
                        <label for="categories">Categories</label>
                        <select
                            name="categories[]" id="categories"
                            class="form-control @error('categories') is-invalid @endif"
                            multiple="multiple"
                        >
                            @foreach($categories as $item)
                                <option @if(in_array($item->id, old('categories') ?? [])) selected
                                        @endif value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>

                        @error('categories')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>


                </div>

                <div class="form-group">
                    <label for="remarks">Remarks <small><em></em></small></label>
                    <textarea name="remarks" id="remarks"
                              class="form-control @error('remarks') is-invalid @endif">{{ old('remarks') ?? "" }}</textarea>

                    @error('remarks')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-plus fa-fw mr-1"></i> Create New Product
                </button>
                <a href="{{ route('admin.products.index') }}" class="btn btn-link float-right"> Cancel</a>
            </div>

        </form>
    </div>

@stop

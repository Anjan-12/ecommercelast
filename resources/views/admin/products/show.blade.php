@extends('adminlte::page')

@section('title', 'Product')

@section('js')
    <script type="text/javascript">
        function deleteTag(id) {
            if (confirm('Are you sure you want to delete this tag?')) {
                document.querySelector('#delete-' + id).submit();
            }
        }
    </script>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size:1.5em;font-weight: bold">
                Product Info
            </h3>
            <div class="card-tools">



                    <i class="fas fa-clipboard-list mr-1"></i> Categories
                </a>






                <a class="btn btn-success btn-sm" href="{{ route('admin.products.index') }}">
                    <i class="fas fa-arrow-left fa-fw mr-1"></i> Go Back
                </a>
            </div>
        </div>

        <div class="card-body p-0">

            <table class="table table-bordered">
                <tr>
                    <th>ID/SKU</th>
                    <td>{{ $product->id }} / {{ $product->sku }}</td>
                </tr>
                <tr>

                    <th>Name</th>
                    <td><b>{{ $product->name }}</b></td>
                </tr>
                <tr>
                    <th>Price</th>
                    <td>{{ env("APP_CURRENCY", "Rs.") }} {{ $product->price }}</td>
                </tr>
                <tr>
                    <th>On Sale?</th>
                    <td>{{ $product->on_sale ? "Yes" : "No" }}</td>
                </tr>
                @if($product->on_sale)
                    <tr>
                        <th>Sale Price</th>
                        <td>{{ env("APP_CURRENCY", "Rs.") }} {{ $product->sale_price }}</td>
                    </tr>
                @endif
                <tr>
                    <th>Description</th>
                    <td>{{ $product->description }}</td>
                </tr>
                @if($product->min_order_quantity)
                    <tr>
                        <th>Minimum Order</th>
                        <td>{{ $product->min_order_quantity }}</td>
                    </tr>
                @endif




            </table>
        </div>
    </div>
@stop

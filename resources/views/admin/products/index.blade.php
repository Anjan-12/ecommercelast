@extends('adminlte::page')

@section('title', 'Products')

@section('content')

    <x-alert/>
    <x-delete/>

    <div class="card">

        <div class="card-body p-0">
            <table class="table table-bordered border-top-0">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>SKU</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th style="width: 275px">Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->sku }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{!! $product->currentprice() !!}</td>
                        <td>
                            <a href="{{ route('admin.products.show',$product->id) }}" class="btn btn-sm btn-primary">
                                <i class="fas fa-fw fa-eye mr-1"></i> Show
                            </a>

                            <a href="{{route('admin.products.edit',$product->id)}}"
                               class="ml-1 btn btn-sm btn-secondary">
                                <i class="fas fa-fw fa-edit mr-1"></i> Edit
                            </a>

                            <a href="#" onclick="confirmDelete({{$product->id}})" class="ml-1 btn btn-sm btn-danger">
                                <i class="fas fa-fw fa-trash mr-1"></i> Delete
                            </a>

                            <form style="display:none" method="POST" id="delete-form-{{ $product->id }}"
                                  action="{{ route('admin.products.destroy', $product->id) }}">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if(count($products) == 0)
                <div class="alert alert-warning mb-0 text-center" style="border-radius: 0">
                    @if(!empty($search))
                        There are no products matching your search term. Please change your search term to something else.
                    @else
                        There are no products registered on the system. Please add some and it will appear here!
                    @endif
                </div>
            @endif
        </div>

        @if($products->total() > 30)
            <div class="card-footer">
                {{ $products->links() }}
            </div>
        @endif

    </div>
@stop

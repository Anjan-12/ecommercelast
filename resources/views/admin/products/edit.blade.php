@extends('adminlte::page')

@section('title', 'Update Product')

@section('content')

    <x-alert/>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size:1.5em;font-weight: bold">Update Product</h3>
            <div class="card-tools">
                <a class="btn btn-success btn-sm" href="{{ route('admin.products.index') }}">
                    <i class="fas fa-arrow-left fa-fw mr-1"></i> Go Back
                </a>
            </div>
        </div>
        <div class="card-body">

            <form action="{{ route('admin.products.update', $product->id) }}" method="post">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="sku">SKU</label>
                    <input
                        type="text"
                        name="sku" id="sku"
                        class="form-control @error('sku') is-invalid @endif"
                        value="{{ old('sku') ?? $product->sku }}"
                        autofocus
                    >
                    @error('sku')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="name">Name</label>
                    <input
                        type="text"
                        name="name" id="name"
                        class="form-control @error('name') is-invalid @endif"
                        value="{{ old('name') ?? $product->name }}"
                    >

                    @error('name')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="slug">Slug <span title="Required" class="text-danger">*</span></label>
                    <input
                        type="text"
                        name="slug"
                        id="slug"
                        class="form-control @error('slug') is-invalid @endif"
                        value="{{ old('slug') ?? $product->slug }}"
                    >

                    @error('slug')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="name">Description</label>
                    <textarea
                        name="description" id="description"
                        class="form-control @error('description') is-invalid @endif"
                    >{{ old('description') ?? $product->description }}</textarea>

                    @error('description')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="price">Price</label>
                    <input
                        type="number"
                        name="price" id="price"
                        class="form-control @error('price') is-invalid @endif"
                        value="{{ old('price') ?? $product->price }}"
                    >

                    @error('price')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="on_sale">
                        <input
                            type="checkbox"
                            name="on_sale" id="on_sale"
                            value="1" @if(old('on_sale') || $product->on_sale) checked @endif
                        >
                        <span>On Sale ?</span>
                    </label>

                    @error('on_sale')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="sale_price">Sale Price</label>
                    <input
                        type="number"
                        name="sale_price" id="sale_price"
                        class="form-control @error('sale_price') is-invalid @endif"
                        value="{{ old('sale_price') ?? $product->sale_price }}"
                    >

                    @error('sale_price')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="on_stock">
                        <input
                            type="checkbox"
                            name="on_stock" id="on_stock"
                            value="1" @if(old('on_stock') || $product->on_stock) checked @endif
                        >
                        <span>On Stock ?</span>
                    </label>

                    @error('on_stock')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="min_order_quantity">Minimum Order Quantity</label>
                    <input
                        type="number"
                        name="min_order_quantity" id="min_order_quantity"
                        class="form-control @error('min_order_quantity') is-invalid @endif"
                        value="{{ old('min_order_quantity') ?? $product->min_order_quantity}}"
                    >

                    @error('min_order_quantity')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="is_adult">
                        <input
                            type="checkbox"
                            name="is_adult" id="is_adult"
                            value="1" @if(old('is_adult') || $product->is_adult) checked @endif
                        >
                        <span>Is Adult ?</span>
                    </label>
                    @error('is_adult')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="remarks">Remarks</label>
                    <textarea
                        name="remarks" id="remarks"
                        class="form-control @error('remarks') is-invalid @endif"
                    >{{ old('remarks') ?? $product->remarks}}</textarea>

                    @error('remarks')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>


                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-edit fa-fw mr-1"></i> Update Product
                </button>
                <a href="{{ route('admin.products.index') }}" class="btn btn-link float-right"> Cancel</a>
            </form>

        </div>
    </div>

@stop

<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Product\UpdateProductRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' =>  'nullable',
            'name' => 'required',
            'slug' => 'required',
            'description' => 'nullable',
            'price' => 'required|numeric|min:0',
            'on_sale' => 'nullable|boolean',
            'sale_price' => 'nullable|numeric|min:0|lt:price|required_if:on_sale,1',
            'on_stock' => 'boolean',
            'min_order_quantity' => 'nullable',
            'is_adult' => 'boolean',
            'remarks' => 'nullable',
        ];
    }
}

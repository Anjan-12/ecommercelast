<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tag;
use App\Models\Media;
use App\Models\Product;
use App\Models\Category;
use App\Models\Attribute;
use Illuminate\Support\Str;
use App\Models\MediaProduct;
use Illuminate\Http\Request;
use App\Models\ProductVariant;
use App\Services\MediaService;
use App\Models\AttributeProduct;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(30);

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::select(['id', 'name'])->get();

        return view('admin.products.create', compact('categories'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $request['slug'] = Str::slug($request->slug);
        $request['sku'] = strtoupper(Str::slug($request->sku));

        $request->validate([
            'slug' => 'required|unique:products,slug',
            'sku' => 'required|unique:products,sku',
        ]);

        $product = Product::create([
            'sku' =>  $request->sku,
            'name' => $request->name,
            'slug' => $request->slug,
            'description' => $request->description,
            'price' => $request->price,
            'on_sale' => $request->on_sale ? true : false,
            'sale_price' => $request->sale_price,
            'on_stock' => $request->on_stock ? true : false,
            'min_order_quantity' => $request->min_order_quantity,
            'is_adult' => $request->is_adult ? true : false,
            'remarks' => $request->remarks,
        ]);



        return redirect()
            ->route('admin.products.index')
            ->with('success', 'Products has been added sucessfully!');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {

        return view('admin.products.show', compact('product'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.products.edit', compact('product'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $request['slug'] = Str::slug($request->slug);
        $request['sku'] = strtoupper(Str::slug($request->sku));

        $request->validate([
            'slug' => 'required|unique:products,slug,' . $product->id,
            'sku' => 'required|unique:products,sku,' . $product->id,
        ]);

        $product->update([
            'sku' =>  $request->sku,
            'name' => $request->name,
            'slug' => $request->slug,
            'description' => $request->description,
            'price' => $request->price,
            'on_sale' => $request->on_sale ? true : false,
            'sale_price' => $request->sale_price,
            'on_stock' => $request->on_stock ? true : false,
            'min_order_quantity' => $request->min_order_quantity,
            'is_adult' => $request->is_adult ? true : false,
            'remarks' => $request->remarks,
        ]);

        return redirect()
            ->route('admin.products.index')
            ->with('success', 'Product has been updated sucessfully!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()
            ->route('admin.products.index')
            ->with('success', 'Product has been deleted sucessfully!');
    }




    public function categories(Product $product)
    {
        $categories = Category::all();
        return view('admin.products.categories', compact('product', 'categories'));
    }

    public function addcategories(Request $request, Product $product)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id'
        ]);

        $product->categories()->attach(Category::find($request->category_id));

        return redirect()
            ->back()
            ->with('sucess', 'Category added sucessfully!');
    }

    public function removecategories(Request $request, Product $product)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id'
        ]);

        $product->categories()->detach(Category::find($request->category_id));

        return redirect()
            ->back()
            ->with('success', 'Category removed sucessfully!');
    }

    /**
     * Search Products
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function search(Request $request)
    {
        if (!$request->has('search') || empty($request->search))
            return redirect()->route('admin.products.index');

        $search = $request->search;

        $products = Product::where('name', 'LIKE', '%' . $search . '%')
            ->orwhere('sku', 'LIKE', '%' . $search . '%')
            ->orderBy('id', 'desc')
            ->paginate(30);

        return view('admin.products.index', [
            'search' => true,
            'products' => $products,
            'search_term' => $search,
        ]);
    }

}
